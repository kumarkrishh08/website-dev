var allDFCustomElements = {};

const getDfMessengerChat = () => {
  return allDFCustomElements['df-messenger-chat'];
};

const getDfMessengerTitlebar = () => {
  return allDFCustomElements['df-messenger-titlebar'];
} 

const getDfMessengerList = () => {
  return allDFCustomElements['df-message-list'];
};

const getDfMessengerUserInput = () => {
  return allDFCustomElements['df-messenger-user-input'];
}

const svgRefreshElement = () => {
  return `<svg fill="#fff" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" width="25px" height="25px"><path d="M 15 3 C 12.031398 3 9.3028202 4.0834384 7.2070312 5.875 A 1.0001 1.0001 0 1 0 8.5058594 7.3945312 C 10.25407 5.9000929 12.516602 5 15 5 C 20.19656 5 24.450989 8.9379267 24.951172 14 L 22 14 L 26 20 L 30 14 L 26.949219 14 C 26.437925 7.8516588 21.277839 3 15 3 z M 4 10 L 0 16 L 3.0507812 16 C 3.562075 22.148341 8.7221607 27 15 27 C 17.968602 27 20.69718 25.916562 22.792969 24.125 A 1.0001 1.0001 0 1 0 21.494141 22.605469 C 19.74593 24.099907 17.483398 25 15 25 C 9.80344 25 5.5490109 21.062074 5.0488281 16 L 8 16 L 4 10 z"/></svg>`
}

const svgMaximizeElement = () => {
  return `<svg fill="#fff" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" width="25px" height="25px"><path d="M4.5 5.75C4.5 5.05964 5.05964 4.5 5.75 4.5H7.75C8.16421 4.5 8.5 4.16421 8.5 3.75C8.5 3.33579 8.16421 3 7.75 3H5.75C4.23122 3 3 4.23122 3 5.75V7.75C3 8.16421 3.33579 8.5 3.75 8.5C4.16421 8.5 4.5 8.16421 4.5 7.75V5.75Z"/><path d="M4.5 18.25C4.5 18.9404 5.05964 19.5 5.75 19.5H7.75C8.16421 19.5 8.5 19.8358 8.5 20.25C8.5 20.6642 8.16421 21 7.75 21H5.75C4.23122 21 3 19.7688 3 18.25V16.25C3 15.8358 3.33579 15.5 3.75 15.5C4.16421 15.5 4.5 15.8358 4.5 16.25V18.25Z"/><path d="M18.25 4.5C18.9404 4.5 19.5 5.05964 19.5 5.75V7.75C19.5 8.16421 19.8358 8.5 20.25 8.5C20.6642 8.5 21 8.16421 21 7.75V5.75C21 4.23122 19.7688 3 18.25 3H16.25C15.8358 3 15.5 3.33579 15.5 3.75C15.5 4.16421 15.8358 4.5 16.25 4.5H18.25Z"/><path d="M19.5 18.25C19.5 18.9404 18.9404 19.5 18.25 19.5H16.25C15.8358 19.5 15.5 19.8358 15.5 20.25C15.5 20.6642 15.8358 21 16.25 21H18.25C19.7688 21 21 19.7688 21 18.25V16.25C21 15.8358 20.6642 15.5 20.25 15.5C19.8358 15.5 19.5 15.8358 19.5 16.25V18.25Z"/></svg>`
}

const svgMinimizeElement = () => {
  return `<svg fill="#fff" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" width="25px" height="25px"><path d="M8 3v3a2 2 0 0 1-2 2H3m18 0h-3a2 2 0 0 1-2-2V3m0 18v-3a2 2 0 0 1 2-2h3M3 16h3a2 2 0 0 1 2 2v3"/></svg>`
}

const isCustomElement = (el) => {
  return el.localName.includes("-");
}

const findAllDFMessengerElements = (nodes, allDFCustomElements = {}) => {
  for (let i = 0, el; (el = nodes[i]); ++i) {
    if (el.shadowRoot) {
      isCustomElement(el) &&
        (allDFCustomElements[el.localName] = el.shadowRoot);
      findAllDFMessengerElements(
        el.shadowRoot.querySelectorAll("*"),
        allDFCustomElements
      );
    }
  }
  return allDFCustomElements;
}

const initChatFontSize = () => {
  const dfMessageList = getDfMessengerList();
  const dfMessagerUserInput = getDfMessengerUserInput();
  const dfMessengerTitlebar = getDfMessengerTitlebar();

  if (!dfMessageList || !dfMessagerUserInput || !dfMessengerTitlebar) return;

  const messageListStyle = dfMessageList.querySelector('style');
  const messagerUserInputStyle =  dfMessagerUserInput.querySelector('style');
  const messengerTitlebarStyle =  dfMessengerTitlebar.querySelector('style');

  let messageListStyleTxt = messageListStyle.innerText;
  let messagerUserInputStyleTxt = messagerUserInputStyle.innerText;
  let messengerTitlebarStyleTxt = messengerTitlebarStyle.innerText;

  messageListStyleTxt += `
    #messageList .message {
      font-size: calc(var(--df-messenger-message-font-size, 14) * 1px);
    }
    #messageList #typing {
      font-size: calc(var(--df-messenger-message-font-size, 14) * 1px);
    }
  `;

  messagerUserInputStyleTxt += `
    .input-container input {
      font-size: calc(var(--df-messenger-input-placeholder-font-size, 14) * 1px);
    }
    #sendIcon:hover {
      fill: var(--df-messenger-send-icon);
    }
  `;

  messengerTitlebarStyleTxt += `
    .title-wrapper {
      padding-right: 15px;
      font-size: calc(var(--df-messenger-title-font-size, 18) * 1px);
    }
    .title-wrapper > :first-child {
      margin-right: auto;
    }
    .title-wrapper > :not(:first-child) {
      cursor: pointer;
    }
    .title-wrapper #maxMinIcon {
      display: none;
    }
    @media screen and (min-width: 501px) {
      .title-wrapper #maxMinIcon {
        display: block;
      }
    }
  `;

  messageListStyle.innerHTML = messageListStyleTxt;
  messagerUserInputStyle.innerHTML = messagerUserInputStyleTxt;
  messengerTitlebarStyle.innerHTML = messengerTitlebarStyleTxt;
}

const initChatWidthAndHeight = () => {
  const dfMessageChat = getDfMessengerChat();
  if (!dfMessageChat) return;

  const chatWrapperStyle = dfMessageChat.querySelector('style');

  let chatWrapperStyleTxt = chatWrapperStyle.innerText;

  chatWrapperStyleTxt += `
    @media screen and (min-width: 501px) {
      div.chat-wrapper[opened="true"] {
        width: calc(var(--df-messenger-width, 370) * 1px);
        height: calc(var(--df-messenger-height, 560) * 1px);
      }
    }
  `;

  chatWrapperStyle.innerHTML = chatWrapperStyleTxt;
}

const initChatRefreshButton = () => {
  const titleBar = getDfMessengerTitlebar();

  if (!titleBar) return;

  const titleWrapper = titleBar.querySelector('.title-wrapper');

  const refreshElem = document.createElement('div');
  refreshElem.id = "refreshIcon";
  refreshElem.innerHTML = svgRefreshElement();

  refreshElem.addEventListener('click', () => {
    dfMessenger.initializeDFMessenger_();
  });

  titleWrapper.appendChild(refreshElem);
}

const initChatMaximizeButton = () => {
  const titleBar = getDfMessengerTitlebar();
  const dfMessageChat = getDfMessengerChat();

  if (!titleBar || !dfMessageChat) return;

  const titleWrapper = titleBar.querySelector('.title-wrapper');
  const chatWrapper = dfMessageChat.querySelector('.chat-wrapper');

  const maxMinIcon = document.createElement('div');
  maxMinIcon.id = "maxMinIcon";
  maxMinIcon.style.marginTop = "6px";
  maxMinIcon.style.marginRight = "10px";

  maxMinIcon.dataset.maximized = false;
  maxMinIcon.innerHTML = svgMaximizeElement();

  maxMinIcon.addEventListener('click', (event) => {
    let currentTarget = event.currentTarget;
    let maximized = event.currentTarget.dataset.maximized === "false" ? true : false;

    if (maximized) {
      chatWrapper.style.width = '98%';
      chatWrapper.style.height = 'calc(100% - 105px)';

      currentTarget.innerHTML = svgMinimizeElement();
    } else {
      chatWrapper.style.width = null;
      chatWrapper.style.height = null;

      currentTarget.innerHTML = svgMaximizeElement();
    }

    currentTarget.dataset.maximized = maximized;
  });

  titleWrapper.appendChild(maxMinIcon);
}

const dfMessenger = document.querySelector("df-messenger");

dfMessenger.addEventListener("df-messenger-loaded", function (event) {
  const nodes = event.target.shadowRoot.querySelectorAll("*");
  allDFCustomElements = findAllDFMessengerElements(nodes);

  initChatFontSize();
  initChatWidthAndHeight();
  initChatMaximizeButton();
  initChatRefreshButton();
});

